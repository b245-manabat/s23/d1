/*console.log("Send Stars!");*/

//[Section]Objects
	//an object is a data type that is used to represent world objects.
	//it is a collection of related data and/or functionalities or method.
	//information stored in objects are represented in "key:value" pair
	//usually we call as property
	//different data type may be stored in an object's property creating data structures.

//Creating objects using object initializer/ object literal notation.

	/*
		let/const objectName = {
			keyA: valueA,
			keyB: valueB,
			. . . 
		}

			// this creates/declares an object and also initializes/assign its properties upon creation
	*/


let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999,
	features: ["send message", 'make call', 'play games'],
	isWorking: true,
	owner: {
		fullName: "Chris",
		yearOfOwnage: 20
	}
}

console.log(cellphone);

	//Accessing/reading properties
		//Dot notation
			/*Syntax:
				objectName.propertyName;
			*/
	
	console.log("accessing using dot notation")
	console.log(cellphone.name);
	console.log(cellphone.isWorking);
	console.log(cellphone.features);
	console.log(cellphone.features[1]);

	console.log(cellphone.owner);
	console.log(cellphone.owner.fullName);
		//bracket Notation
	console.log("accessing using bracket notation")
	console.log(cellphone["name"]);
	console.log(cellphone["owner"]["fullName"])

	//Creating objects using constructor function

	/*
		-creates a reusable function to create several objects that have the same data structure
		-This is useful for creating multiple instances/copies of an object
		-instance is a concrate occurence of any object which emphasize distinct/unique identity.
		Syntax:
			function objectName(valueA,valueB){
				this.propertyA = valueA,
				this.propertB= valueB
			}
	
	*/
	//This is a constructor function.
		function Laptop(name, manufactureDate, ram, isWorking){
			
			//this keyword allows us to assign object properties by associating them with the values received from a constructor function's parameter

			this.laptopName = name,
			this.laptopManufactureDate = manufactureDate,
			this.laptopRam = ram,
			this.isWorking = isWorking
		}

		//Instatiation
		//The "new" keyword creates an instance of an object
		let laptop = new Laptop("Lenovo", 2008, "2g b", true)
		console.log(laptop);

		let myLaptop = new Laptop("Macbook Air", 2020, "8 gb", false);
		console.log(myLaptop);

		let oldLaptop = new Laptop("Portal R2E CCMC", 1980, "500 mb", false)
		console.log(oldLaptop);

		function Menu(mealName, mealPrice){
			this.menuName = mealName,
			this.menuPrice = mealPrice
		}

		let mealOne = new Menu("Breakfast", 299);

		console.log(mealOne);

		let mealTwo = new Menu("Dinner", 500);
		console.log(mealTwo);


		//Create empty objects
		let computer = {};
		let emptyObject = new Object()
		console.log(computer)
		console.log(emptyObject)

		//Access objects inside an array
		let array = [laptop, myLaptop];
		console.log(array);

		//get the value of the property laptopName in the first element of our object.

		console.log(array[0].laptopName);

		console.log(array[1].laptopManufactureDate);

	//[Section] Initialization/adding/deleting/reassigning Object properties

		/*
			like any other variable in JavaScript, objects have their properties initialized or added after the object was created/ declared
		*/

	let car = {};
	console.log(car)

		//Initialize/ add object property using dot notation
			/*
				Syntax: objectName.propertyToBeAdded = value;
			*/
		car.name = "Honda Civic";
	console.log(car);

		//Initialize/ add object using bracket notation
			/*
				objectName["propetyToBeAdded"] = value;
			*/
		car["manufactureDate"] = 2019;
	console.log(car);	

		//Deleting Object Properties
			//deleting using dot notation
			/*
				Syntax:
				delete objectName.propertyToBeDeleted;
			*/

		delete car.name;
		console.log(car);

			//deleting using bracket notation
			/*
				Syntax:
					delete objectName["propertyToBeDeleted"]
			*/

		delete car["manufactureDate"];
		console.log(car);

		//Reassigning object properties
		car.name = "Honda Civic";
		car["manufactureDate"] = 2019;
		console.log(car)

			//Reassigning object propert using dot notation
				car.name = "Dodge Charger R/T";
			console.log(car)

			//Reassigning object property using bracket notation
				car["name"] = "Jeepney";
			console.log(car);

		//[Reminder] If you will add a new property to the object make sure that the property name that you are going to add is not existing or else it will just reassign the value of the property.


	//[Section] Object Methods
			// A method is a function which is a property of an object
			//they are also functions and one of the key differences the have is thatmethods are fuunctions related to a specific object.

	let person = {
		name: "John Doe",
		talk: function(){
			console.log("Hello my name is " + this.name)
		}
	}

	console.log(person);

	person.talk();

	//add method to object
	person.walk = function(){
		console.log(person.name + " walked 25 steps forward.")
	}

	console.log(person);
	person.walk();

	//methods are useful for creating reusable functions that perform tasks related to objects.

	//constructor function with method

	function Pokemon(name, level){
		//Properties of the object
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		// Method
		this.tackle = function(targetPokemon){
			console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
			
			let hpAftertackle = targetPokemon.pokemonHealth- this.pokemonAttack;
			
			console.log(targetPokemon.pokemonName + " health is now reduced tp " + hpAftertackle);
		}

		this.faint = function(){
			console.log(this.pokemonName + " fainted!")
		}

	}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu)
	let gyarados = new Pokemon("Gyarados", 20);
	console.log(gyarados)

	pikachu.tackle(gyarados);

	gyarados.tackle(pikachu);

	pikachu.faint();

